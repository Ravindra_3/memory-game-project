const express =require("express")


const app = express()
app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html')
})
app.get('/css', function (req, res) {
  res.sendFile(__dirname + '/style.css')
})
app.get('/js', function (req, res) {
    res.sendFile(__dirname + '/script.js')
  })
app.use('/static', express.static('public'))
.listen(process.env.PORT, () => {
    console.log("server started"+ process.env.PORT);
  });